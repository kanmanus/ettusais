<?php
namespace App\Http\Controllers\frontend;

use App\User;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  -
     * @return Response blade views
     */
    public function main()
    {
        return view('frontend.index');
    }
    public function premium(){
        return view('frontend.premium');
    }
    public function skincare(){
        return view('frontend.skincare');
    }
}
