<?php
use App\Http\Middleware\Cors;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
*
*   FRONT END ROUTE
*
*/
Route::get('/',[
  'as'=>'home',
  'uses'=>'frontend\MainController@main'
]);
Route::get('premium',[
  'as'=>'premium',
  'uses'=>'frontend\MainController@premium'
]);
Route::get('skincare',[
  'as'=>'premium',
  'uses'=>'frontend\MainController@skincare'
]);


Route::group(array('middleware' => ['cors','web'], 'before' => '', 'prefix' => 'api'), function(){
    //API webservice route

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['prefix'=>'_admin'],function() {

    Route::get('/',['middleware'=>'admin',function(){
        return view('backend.index');
    }]);

    Route::get('login','Backend\AuthController@getLogin');
    Route::post('login','Backend\AuthController@postLogin');
    Route::get('logout','Backend\AuthController@getLogout');

});
