var elixir = require('laravel-elixir');
var livereload = require('gulp-livereload');
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
  /*
   |--------------------------------------------------------------------------
   | Elixir Asset Management
   |--------------------------------------------------------------------------
   |
   | Elixir provides a clean, fluent API for defining some basic Gulp tasks
   | for your Laravel application. By default, we are compiling the Sass
   | file for our application, as well as publishing vendor resources.
   |
   */

elixir(function(mix) {
    mix.task('multiple-sass', 'resources/assets/sass/front/*.scss');
    mix.sass('resources/assets/sass/front/global/*.scss', 'public/css/front/app.css')
        .browserify('front/main.js','public/js/front/main.js')
        .browserSync({
          proxy: 'http://localhost/ettusais/public' // edit proxy server for development here ขี้ path ไปที่ Apache
        });
});

gulp.task('multiple-sass', function(){
    return gulp.src('resources/assets/sass/front/*.scss')
                .pipe(sass())
                .pipe(autoprefixer())
                .pipe(gulp.dest('public/css/front'));
});
