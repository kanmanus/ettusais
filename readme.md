## BKKSOL  Laravel PHP Framework PROTOTYPE



Clean Laravel project ที่ implement backend prototype สำหรับทำ full stack php web application ที่ Bangkoksolutions 

## วิธีใช้

Clone เวอร์ชั่นล่าสุดแล้ว run ใน apache server 

## Requirement

Apache or nginx server
PHP Engine 5.5+
MySQL database

## Develop requirement
NodeJS npm composer

## Develop automate command
โปรเจคนี้ใช้ Laravel Elixir 
compile file : gulp
watch file changed : gulp watch
production build : gulp --production


### License and External library use

The Laravel framework is open-sourced software licensed under the [MIT license].
Bootstrap Framework : The MIT License (MIT) Copyright (c) 2011-2016 Twitter, Inc.