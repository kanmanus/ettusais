<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES 1-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/uniform.default.css') }}" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ URL::asset('assets/admin/pages/css/login.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/components-rounded.css') }}" id="style_components" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/plugins.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/layout.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/default.css') }}" rel="stylesheet" type="text/css" id="style_color">
    <link href="{{ URL::asset('js/back/bkksol_backend_dependencies/src/custom.css') }}" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}"/>

</head>
<body class="login">

<!-- BEGIN CONTENT -->
@yield('content')
        <!-- BEGIN CONTENT -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ URL::asset('assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery.cokie.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/jquery.validate.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/metronic.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/layout.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/demo.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/back/bkksol_backend_dependencies/src/login.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init(); // init demo features
    });
</script>
<!-- END JAVASCRIPTS -->

</body>
</html>
