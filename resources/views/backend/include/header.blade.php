




<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{URL::to('_admin')}}"><img src="{{ URL::asset('assets/admin/layout3/img/logo_bkksol.jpg') }}" alt="logo" class="logo-default"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" >
                            <span class="username username-hide-mobile">Hello, {{auth()->guard('admin')->user()->firstname}}</span>
                        </a>
                    </li>
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="{{URL::to('_admin/logout')}}" >
                            <span class="username username-hide-mobile"><i class="fa fa-sign-out"></i> Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">

                    <li><a href="#"><i class="fa fa-reorder"></i> Menu without dropdown</a></li>

                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-reorder"></i> Menu with dropdown<i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">

                            <li class=""><a href="#"><i class="fa fa-reorder"></i> Menu 1</a></li>
                            <li class=""><a href="#"><i class="fa fa-reorder"></i> Menu 2</a></li>
                            <li class=""><a href="#"><i class="fa fa-reorder"></i> Menu 3</a></li>
                        </ul>
                    </li>

                    {{--<li><a href="{{ URL::to('_admin/call_center') }}"><i class="fa fa-phone"></i> Call Center</a></li>--}}

                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>

                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>

