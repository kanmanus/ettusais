@extends('frontend.layouts/main')


@section('more-stylesheet')
  <link rel="stylesheet" href="{{URL::asset('css/front/home.css')}}">
  <link rel="stylesheet" href="{{URL::asset('tools/bxslider/jquery.bxslider.css')}}">
@endsection
@section('title','Home')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}">
@endsection

@section('content')

  <div class="container">
    <section id="slider">
      <ul id="bxslider1">
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
        <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
      </ul>
      <div id="bx-pager" class="hidden-md hidden-sm hidden-xs">
        <a data-slide-index="0" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
        <a data-slide-index="1" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
        <a data-slide-index="2" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
        <a data-slide-index="0" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
        <a data-slide-index="1" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
        <a data-slide-index="2" href=""><img src="{{ URL::asset('images/tmp-slide1.png') }}" /></a>
      </div>
    </section>
    <section id="banner2" class="row">

    <div id="white" class="hidden-lg hidden-sm hidden-md"></div>
      <div class="col-md-12">
          <div id="detail-banner2">
            <img src="{{ URL::asset('images/Logo_PremiumBanner.png') }}">
            <p class="text-center"><b>For the future that your skin dream of</b></p>
            <p class="text-center">เพื่อผิวสวยใส ดุจย้อนวัยเด็ก</p>
          </div>
        </div>
    
    </section>
 
    <section id="seller" class="hidden-xs">
      <div class="row">
        <div class="col-md-12">
          <h1>BEST SELLER ITEMS</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ul id="bxslider2">
            <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide2.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide3.png') }}" /></a></li>
          </ul>
        </div>
      </div>

    </section>

    <section id="seller2" class="hidden-lg hidden-sm hidden-md">
      <div class="row">
        <div class="col-md-12">
          <h1>BEST SELLER ITEMS</h1>
        </div>
      </div>
   
        <div class="col-sm-12 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide3.png') }}" />
        </div>
        <div class="col-sm-12 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide3.png') }}" />
        </div>
    
        <div class="col-sm-12 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide3.png') }}" />
        
      </div>

    </section>

    <section id="recommend" class="hidden-xs">
      <div class="row">
        <div class="col-md-12">
          <h1>RECOMMEND PRODUCTS</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ul id="bxslider3">
            <li><a href="#"><img src="{{ URL::asset('images/slide4.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide5.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide6.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide1.png') }}" /></a></li>
            <li><a href="#"><img src="{{ URL::asset('images/slide3.png') }}" /></a></li>
          </ul>
        </div>
      </div>

    </section>

    <section id="recommend2" class="hidden-lg hidden-sm hidden-md">
      <div class="row">
        <div class="col-md-12">
          <h1>RECOMMEND PRODUCTS</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide4.png') }}" />
        </div>
        <div class="col-xs-6 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide5.png') }}" />
        </div>
      </div>
      <div class="row" style="margin-top:10px;">
        <div class="col-xs-6 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide5.png') }}" />
        </div>
        <div class="col-xs-6 text-center">
          <img class="img-responsive" src="{{ URL::asset('images/slide4.png') }}" />
        </div>
      </div>

    </section>

    <section id="event">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2 col-md-offset-0 col-md-6 col-md-push-6">
          <h1>NEWS & EVENTS</h1>
          <a href="#" class="row">
            <div class="col-md-12">3/10（木） ふきとりピーリングシートN WEB先行発売!</div>
          </a>
          <a href="#" class="row">
            <div class="col-md-12">3/10（木） オイルブロックベース WEB先行発売!</div>
          </a>
          <a href="#" class="row">
            <div class="col-md-12">2/23（火） プレミアム CCアミノクリーム BE(シアーベージュ)</div>
          </a>
          <a href="#" class="row">
            <div class="col-md-12">3/10（木） ふきとりピーリングシートN </div>
          </a>
          <a href="#" class="row">
            <div class="col-md-12">2/23（火） プレミアム CCアミノクリーム BE(シアーベージュ)</div>
          </a>
         

        </div>
        <div class="col-xs-12 col-md-6 col-md-pull-6">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/5SBHyYaqmMw" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
       
    </section>

  </div>

@endsection
@section('more-script')
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="{{URL::asset('tools/bxslider/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript">

  $(document).ready(function(){
    $('#bxslider1').bxSlider({
      pagerCustom: '#bx-pager'
    });
    $('#bxslider2').bxSlider({
      slideWidth: 700,
      minSlides: 2,
      maxSlides: 2,
      slideMargin: 10
    });
    $('#bxslider3').bxSlider({
      slideWidth: 600,
      minSlides: 3,
      maxSlides: 3,
      slideMargin: 0
    });

   
  });

</script>
@endsection
