<footer>
    <div id="full-footer" class="container hidden-xs hidden-sm">
        <!-- Origional snipet by msurguy<http://bootsnipp.com/msurguy>, I editted his snipet so that it works with bootstrap 3.3 -->
<div class="container">
    <div class="row">
            <div class="col-xs-3">
                <ul class="list-unstyled">
                    <li><a href="#">Ettusais Premium</a></li>
                    <li><a href="#">Skincare</a></li>
                    <li><a href="#">Base Care</a></li>
                    <li><a href="#">Point Makeup</a></li>
                </ul>
            </div>
            <div class="col-xs-3">
                <ul class="list-unstyled">
                    <li><a href="#">Ettusais Avenue</a></li>
                    <li><a href="#">Beauty Tips</a></li>
                    <li><a href="#">Special</a></li>
                    <li><a href="#">Talk With</a></li>
                </ul>
            </div>
            <div class="col-xs-3">
                <ul class="list-unstyled">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Ettusais Shop</a></li>
                    <li><a href="#">Talk With</a></li>
                </ul>
            </div>
    </div>
    <hr>
    <div class="row">
            <div class="col-xs-8">
                <ul class="list-unstyled list-inline pull-left">
                    <li><a href="#">Terms of Service</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Privacy</a></li>
                </ul>
            </div>
            <div class="col-xs-4">
                <p class="text-muted pull-right" id="copyright">© copyright 2016 ettusais co.,ltd All Rights Reserved.</p>
            </div>
        </div>
</div>
    </div>
</footer>
