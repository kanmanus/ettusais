

<header>
    <div class="container">
        <a href="{{ URL::route('home') }}" id="logo">
            <img src="{{ URL::asset('images/logo.png') }}" class="image" />
        </a>

        <nav class="hidden-xs">
            <a href="{{ URL::route('home') }}" class="nav-item"><img src="{{ URL::asset('images/store.png') }}" width="20px"> Ettusais Shop</a>
            <a href="{{ URL::route('home') }}" class="nav-item" style="padding:2px;"><img src="{{ URL::asset('images/facebook.png') }}" width="20px"></a>
            <a href="{{ URL::route('home') }}" class="nav-item" style="padding:2px;"><img src="{{ URL::asset('images/instagram.png') }}" width="20px"></a>
            <ul class="nav-item" style="padding: 20px 30px;text-align: center;">
                <li><a href="#">ตระกร้าสินค้า</a></li>
                <li><a href="#">สมัครสมาชิก</a></li>
            </ul>
            <a href="{{ URL::route('home') }}" class="nav-item" style="padding:2px;"><img src="{{ URL::asset('images/cart.png') }}" id="cart"></a>
        </nav>


        {{--<a href="#" class="another-nav nav-item hidden-xs hidden-sm">--}}
            {{--<i class="glyphicon glyphicon-menu-hamburger"></i> Another Menu--}}
        {{--</a>--}}
    </div>
</header>
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button id="burger" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#">Ettusais Premium</a></li>
            <li><a href="#about">Skincare</a></li>
            <li><a href="#contact">Base Care</a></li>
            <li><a href="#about">Point Makeup</a></li>
            <li><a href="#contact">Ettusais Avenue</a></li>
            <li><a href="#" class="hidden-md hidden-lg hidden-sm">Ettusais Shop</a></li>
            <li><a href="#" class="hidden-md hidden-lg hidden-sm">ตระกร้าสินค้า</a></li>
            <li><a href="#" class="hidden-md hidden-lg hidden-sm">สมัครสมาชิก</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
</nav>