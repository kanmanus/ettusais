
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BKKSOL -  @yield('title')</title>
    <!-- Sass compile result-->
      <link href="{{URL::asset('css/front/app.css')}}" rel="stylesheet">

      @yield('more-stylesheet')
    <!-- END Sass -->

    {{-- Fonts --}}
      <link href="{{URL::asset('css/fonts/fontface.css')}}" rel="stylesheet">
    {{-- END Fonts --}}

  </head>

  <body>
    @include('frontend.layouts.header')
    {{-- Default main content will fill fullscreen to remove this remove fixed-fullscreen className --}}
    <div id="ui-main">
      <div class="main-content">
        @yield('content')
      </div>
    </div>

    @include('frontend.layouts.footer')
  </body>

  <!-- Import script from browserify -->
  <script src="{{URL::asset('js/front/main.js')}}"></script>
  <!-- END -->

  @yield('more-script')

</html>
