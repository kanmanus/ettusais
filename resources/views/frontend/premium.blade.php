@extends('frontend.layouts/main')


@section('more-stylesheet')
  <link rel="stylesheet" href="{{URL::asset('css/front/premium.css')}}">
 
@endsection
@section('title','Home')

@section('content')
    <section id="banner">
      <div class="container">
      <div class="row">

      <div id="detail-banner" class=" col-md-5  ">
            <img src="{{ URL::asset('images/Logo_PremiumBanner.png') }}" width="400px">
            <p class="text-left"><h1>For the future that your skin dream of</h1></p>
            <p style="margin-bottom:25px;"><h2><b>เพื่อผิวสวยใส ดุจย้อนวัยเด็ก</b></h2></p>
            <p><b>กว่า 20 ปี ที่เอต์ตูเซ่ส์</b> ได้คิดค้นพัฒนาผลิตภัณฑ์ดูแลผิวพรรณ โดยเน้นผิวที่
เป็นกังวลต่างๆ เช่น ผิวมัน ผิวแห้ง รูขุมขนกว้าง และปัญหาสิวของผู้หญิงในวัยรุ่น
ไปจนถึงวัย 20 ตอนปลาย จากวันนั้นถึงวันนี้ เราได้เติบโตเป็นผู้ใหญ่ไปพร้อมๆ กัน
และเรายังคงไม่หยุดยั้งที่จะคิดค้นเทคโนโลยีที่ก้าวล้ำล่าสุด เพื่อตอบโจทย์ความต้องการ
ของผู้หญิงที่ย่างก้าวเข้าสู่วัยเลข 3 และเริ่มประสบปัญหาผิวที่ร่วงโรย หย่อนคล้อย
หมองคล้ำ และดูไม่เปล่งประกายความกระจ่างใสเหมือนก่อน</p>
          </div>
        <div class="col-md-5  ">
            <img src="{{ URL::asset('images/product.png') }}" alt="" class="img-responsive" style="margin-top:20px;">
          </div>
          </div>
          </div>
    </section>

    <section id="content">
      <div class="container">
        <!--start product-->
        <div class="row">
          <div class="col-md-8 product-detail">
            <h2>Premium amino Essence Foundation 30ml.</h2>
<p>
สุดยอดรองพื้นเพื่อผิวสวยอ่อนเยาว์และเรียบเนียนอย่างเป็นธรรมชาติ ด้วยนวัตกรรมที่ผสานส่วนผสม
บำรุงผิว Amino Premium 7 ช่วยกักเก็บความชุ่มชื้น และป้องกันการเกิดริ้วรอยก่อนวัย พร้อมปกปิด
รูขุมขนได้อย่างเรียบสนิท โดยไม่ทำให้ผิวหน้าดูหมองคล้ำ </p>
          </div>
          <div class="col-md-4 text-center product-img">
            <img src="{{ URL::asset('images/product2.png') }}" alt="">
          </div>
        </div>
        <div class="row price-item">
          <div class="col-md-3 col-xs-offset-1 col-xs-7">
            <p class="price">ราคา  1,100 บาท</p>
          </div>
          <div class="col-md-5 col-xs-4 addtocart">
            <p><button class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ADD TO CART</button></p>
          </div>

        </div>
        <hr>
        <!--end product-->
<!--start product-->
        <div class="row">
          <div class="col-md-8 product-detail">
            <h2>Premium CC Loose Powder SPF25 PA+++ 11g.</h2>
<p>
แป้งฝุ่น Loose Powder เนื้อเนียนละเอียด ที่มีคุณสมบัติโดดเด่นแบบ CC ช่วยปรับสีผิวให้ดูกระจ่างใส
อมชมพูอย่างเป็นธรรมชาติ พร้อมส่วนผสมบำรุงที่ไม่ทำให้ผิวแห้งกร้าน และช่วยควบคุมความมันส่วน
เกินระหว่างวัน </p>
          </div>
          <div class="col-md-4 text-center product-img">
            <img src="{{ URL::asset('images/product3.png') }}" alt="">
          </div>
        </div>
        <div class="row price-item">
          <div class="col-md-3 col-xs-offset-1 col-xs-7">
            <p class="price">ราคา  1,100 บาท</p>
          </div>
          <div class="col-md-5 col-xs-4 addtocart">
            <p><button class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ADD TO CART</button></p>
          </div>

        </div>
        <hr>
        <!--end product-->
        <!--start product-->
        <div class="row">
          <div class="col-md-8 product-detail">
            <h2>Premium amino caviar cream      ขนาด  90g.</h2>
<p>
 All- in –one เจลครีม ผลิตภัณฑ์ฟื้นฟูบำรุงผิวสวยให้ดูเต่งตึง เปล่งประกายสดใส ชุ่มชื้น ดูอ่อนเยาว์
ด้วยเทคโนโลยี Caviar capsule ที่บรรจุ amino premium 7 อย่างเข้มข้น </p>
          </div>
          <div class="col-md-4 text-center product-img">
            <img src="{{ URL::asset('images/product4.png') }}" alt="">
          </div>
        </div>
        <div class="row price-item">
          <div class="col-md-3 col-xs-offset-1 col-xs-7">
            <p class="price">ราคา  1,500 บาท</p>
          </div>
          <div class="col-md-5 col-xs-4 addtocart">
            <p><button class="btn btn-default"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ADD TO CART</button></p>
          </div>

        </div>
        <hr>
        <!--end product-->
        

      </div>

    </section>

 
@endsection
@section('more-script')
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

@endsection
