@extends('frontend.layouts/main')


@section('more-stylesheet')
  <link rel="stylesheet" href="{{URL::asset('css/front/skincare.css')}}">
 
@endsection
@section('title','Home')

@section('content')
  <section id="head-bar">
    <div class="container">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <p class="navbar-brand">PRODUCTS SKINCARE</p>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="type">
         
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" class="active">ALL</a></li>
            <li><a href="#">NEW</a></li>
            <li><a href="#">RECOMENDED</a></li>
            <li><a href="#">BEST SELLER</a></li>
            
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    </div>
  </section>
  
  <section id="content">
    <div class="container">
      <!--Start ROW Product-->
      <div class="row ">
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product1.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Acne Gentle Make off</h3>
          <p class="price">ราคา 850 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product2.png') }}" class="img-responsive" alt=""> 
          <h3 class="product-name">Acne Gentle Wash</h3>
          <p class="price">ราคา 450 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product3.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Pore care wash</h3>
          <p class="price">ราคา 1,050 บาท</p>
          </div>
         </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product4.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Jelly Mousse</h3>
          <p class="price">ราคา 890 บาท</p>
          </div>
         </div>
      </div>
      <!--End ROW Product-->
      <!--Start ROW Product-->
      <div class="row ">
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product1.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Acne Gentle Make off</h3>
          <p class="price">ราคา 850 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product2.png') }}" class="img-responsive" alt=""> 
          <h3 class="product-name">Acne Gentle Wash</h3>
          <p class="price">ราคา 450 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product3.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Pore care wash</h3>
          <p class="price">ราคา 1,050 บาท</p>
          </div>
         </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product4.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Jelly Mousse</h3>
          <p class="price">ราคา 890 บาท</p>
          </div>
         </div>
      </div>
      <!--End ROW Product-->
      <!--Start ROW Product-->
      <div class="row ">
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product1.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Acne Gentle Make off</h3>
          <p class="price">ราคา 850 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product2.png') }}" class="img-responsive" alt=""> 
          <h3 class="product-name">Acne Gentle Wash</h3>
          <p class="price">ราคา 450 บาท</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product3.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Pore care wash</h3>
          <p class="price">ราคา 1,050 บาท</p>
          </div>
         </div>
        <div class="col-md-3 col-xs-12 col-sm-6 text-center">
          <div class="bottom">
          <img src="{{ URL::asset('images/product4.png') }}" class="img-responsive" alt="">
          <h3 class="product-name">Jelly Mousse</h3>
          <p class="price">ราคา 890 บาท</p>
          </div>
         </div>
      </div>
      <!--End ROW Product-->
     
    
    </div>

  </section>

@endsection
@section('more-script')
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

@endsection
